# WasPageForm

'''bash
cd ../php-wasm
git checkout 953d096
docker buildx bake
cd ../was-page-form

cp ../php-wasm/build/php-web.* .
box compile
docker run \
  -v $(pwd)/phar:/src/phar \
  -v $(pwd):/dist \
  -w /dist \
  soyuka/php-wasm:8.2.9 \
  python3 \
    /emsdk/upstream/emscripten/tools/file_packager.py \
    php-web.data \
      --use-preload-cache \
      --lz4 \
      --preload "/src" \
      --js-output=php-web.data.js \
      --no-node \
      --exclude '*/.*' \
      --export-name=createPhpModule

sed '/--pre-js/r php-web.data.js' php-web.mjs > this-has-preloaded-data-php-web.mjs
mv this-has-preloaded-data-php-web.mjs php-web.mjs

docker run -d --rm -p 8142:80 -v $PWD:/usr/share/caddy/ caddy
'''
